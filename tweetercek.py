
from tkinter import *
from requests_oauthlib import OAuth1 # Uporabimo knjiznico oauth1 za prijavo na twitter sistem
import requests, json # Knjiznica requests in format json (namenjen za izmenjavo podatkov, zapis tweetov)
 
# Kljuci do moje aplikacije na twitterju
ACCESS_KEY = '2978287859-NBoB2btmtQLM9C6y7ZioIAyn2KlCRudkEkt8VcA'
ACCESS_SECRET = 'CwlmwPYg4SzAfyE96pnCcAKFsGLNhndcegebjH4osA3UM'
 
CUSTOMER_KEY = 'tiCzBRn8ZNoW1O33lMYLkrdVz'
CUSTOMER_SECRET = 'GtF41BykWhMhqcnczQ6efek3dhvT2TPERg8xMGhkGWmVji5DJS'
 
authentication = OAuth1(CUSTOMER_KEY, CUSTOMER_SECRET, ACCESS_KEY, ACCESS_SECRET, signature_type='auth_header') # Zgradi prijavo za twitter aplikacijo
 
class Tweetercek:
        def __init__(self,root):
                self.kljucna_beseda=StringVar()
                self.stevilo=StringVar()
                self.stevilo.set(10)
 
                root.title('Tweeterček')
                root.geometry('900x495+250+140')
 
                glava = Frame(root)
                glava.grid(row=0, column=0)
 
                telo = Frame(root)
                telo.grid(row=1, column=0)

                # Drsnika
                yScroll = Scrollbar(telo, orient=VERTICAL)
                yScroll.grid(row=2, column=1, sticky=N+S)
 
                xScroll = Scrollbar(telo, orient=HORIZONTAL)
                xScroll.grid(row=3, column=0, sticky=E+W)

                # Okno za prikaz tvitov
                listbox = Listbox(telo, width=110, height=28, xscrollcommand=xScroll.set, yscrollcommand=yScroll.set) # Nastavimo drsnika
                listbox.grid(row=2, column=0, sticky=N+S+E+W)
 
                vnos = Entry(glava, textvariable=self.kljucna_beseda) # Vnos poizvedbe
                stevilo = Entry(glava, textvariable=self.stevilo) # Vnos zeljenega stevila prikazanih tvitov
                gumb = Button(glava, command=lambda:self.najdi(listbox), text='Najdi!') # Gumb Najdi!
 
                Label(glava, text='Vnesi ključno besedo:').grid(row=1, column=0, padx=3)
                vnos.grid(row=1, column=1, padx=2)
                Label(glava, text='Število prikazanih tvitov:').grid(row=1, column=2, padx=3)
                stevilo.grid(row=1, column=3, padx=3)
                gumb.grid(row=1, column=4)
 
                xScroll['command'] = listbox.xview # Drsnika prilagodimo izpisu tvitov
                yScroll['command'] = listbox.yview
 
                vnos.focus()
                
 
        def najdi(self, listbox):
                n = 10
                listbox.delete(0, END) # Zbrišemo prejšnje prikazane tvite

                # Ulovimo napako, če vnesemo neveljavno število
                try:
                    n = int(self.stevilo.get())
                except:
                    listbox.insert(END, "Error: Count entry is not a number.")
                    return
                
                authentication = OAuth1(CUSTOMER_KEY, CUSTOMER_SECRET, ACCESS_KEY, ACCESS_SECRET, signature_type='auth_header') # Zgradi prijavo za twitter aplikacijo
                query=str(self.kljucna_beseda.get())
                url = 'https://api.twitter.com/1.1/search/tweets.json?q='+query+"&count="+str(n)
                response = requests.get(url, auth=authentication)
                json = response.json()
  
                niz = ""               

                # Ulovimo napako, če ne vnesemo ničesar
                try:
                    for tweet in json['statuses']:
                            try:
                                n = int(self.stevilo.get())
                                display_name = tweet.get('user').get('screen_name')
                                text = tweet.get('text')
                                listbox.insert(END, display_name + ": " + text)
                                listbox.insert(END, '')
                            except:
                                print("Tweet was not displayed because of invalid chars.")
                except:
                    listbox.insert(END, "Tweet was not displayed because of invalid chars.")
                        
                return
 
root = Tk()
app = Tweetercek(root)
root.mainloop()

